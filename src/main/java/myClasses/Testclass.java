package myClasses;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*
 * @author Narcisa Teodora Sirca 
 * narcisa.teodora16@gmail.com
 * 
 * I only used this class while testing my operations, before having GUI implemented 
 */


//2 cum de nu pot da remove>> eror
/* 
 * this class is not included in diagrams or in the documentation because I used it while implementing the 
 * operations to check their correctness. 
 * 
 */
public class Testclass {

	public static void main(String[] args) throws CloneNotSupportedException {
		Polynomial first = new Polynomial();
		Polynomial second = new Polynomial();
		System.out.println("please introduce the polynomial having the form: ax^b+cx^d-e");
		Scanner input = new Scanner(System.in);
		String poly1 = input.nextLine();
		Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
		Matcher m = p.matcher(poly1);
		while (m.find()) {
			// System.out.println("Coef: " + m.group(1));
			// System.out.println("Degree: " + m.group(2));
			first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));

		}
		String poly2 = input.nextLine();
		// Pattern p2 = Pattern.compile( "(-?\\b\\d+)[xX]\\^(-?\\d+\\b)" );
		Matcher m2 = p.matcher(poly2);
		while (m2.find()) {
			// System.out.println("Coef: " + m2.group(1));
			// System.out.println("Degree: " + m2.group(2));
			second.addMonomial(new Monomial(Integer.parseInt(m2.group(2)), Integer.parseInt(m2.group(1))));

		}

		System.out.println("-----------primul polinom--------------");
		System.out.println(first.printPolynomial());
		System.out.println();
		System.out.println("--------al doilea polinom----------------");
		System.out.println(second.printPolynomial());
		System.out.println();
		input.close();
		/*
		System.out.println("-------produsul lor------------------");
		Polynomial product = first.multiplyPolynomials(second);
		System.out.println(product.printPolynomial());
		System.out.println();
		System.out.println("--------suma lor-----------------");
		Polynomial sum = first.addPolynomials(second);
		System.out.println(sum.printPolynomial());
		System.out.println();
		System.out.println("--------suma integrata -----------------");
		//sum.integratePolynomial();
		System.out.println(sum.printPolynomial());
		System.out.println();
		System.out.println("-----------diferenta lor--------------");
		Polynomial diff = first.subtractPolynomials(second);
		System.out.println(diff.printPolynomial());

		System.out.println();
		System.out.println("--------diferenta derivata-----------------");
		//diff.differentiatePolynomial();
		// diff.removeZeroMonomial();
		System.out.println(diff.printPolynomial());
		System.out.println();
		System.out.println("-------------------------"); 
		*/
		first.dividePolynomials(second);
		 System.out.println("-------------------------");
		 ArrayList<Polynomial> impartite=first.dividePolynomials(second);
		//System.out.println("quotient is: "+impartite[0].printPolynomial());
		// System.out.println("reminder is: "+impartite[1].printPolynomial());
		 
		 
		 StringBuffer imparte = new StringBuffer("q: "); for(Polynomial iter: impartite)
		 imparte.append(iter.printPolynomial()+" r: ");
		 imparte.delete(imparte.length()-3, imparte.length());
		 System.out.println(imparte.toString()); 

		/*
		 * 1x^2+3x^1+2x^0 1x^1+2x^0
		 * 
		 * 
		 * System.out.println();
		 * 
		 * 
		 * 
		 * 
		 * 
		 * Polynomial firstD = new Polynomial(); Polynomial secondD = new Polynomial();
		 * Monomial m6 = new Monomial(0, 3); Monomial m7 = new Monomial(1, 3); Monomial
		 * m8 = new Monomial(2, 1); Monomial m9 = new Monomial(0, 2); Monomial m10 = new
		 * Monomial(1, 1);
		 * System.out.println("-----------primul polinom--------------");
		 * firstD.addMonomial(m6); firstD.addMonomial(m7); firstD.addMonomial(m8);
		 * firstD.printPolynomial(); System.out.println(); System.out.println();
		 * System.out.println("--------al doilea polinom----------------");
		 * secondD.addMonomial(m9); secondD.addMonomial(m10); secondD.printPolynomial();
		 * System.out.println();
		 * 
		 * System.out.println("-------------------------");
		 * System.out.println("-------------------------");//aci ma blochez, arunca
		 * exceptii // ArrayList<Polynomial> impartite =
		 * first.dividePolynomials(second); //
		 * System.out.println("-------------------------"); // impartite.toString();
		 * //System.out.println("-------------------------");
		 * //impartite.printPolynomial(); //Polynomial rest =
		 * first.dividePolynomials(second)[1];
		 * //System.out.println("-------------------------"); //rest.printPolynomial();
		 */
		System.out.println();
	}

}
