package myClasses;
/*
 * @author Narcisa Teodora Sirca 
 * narcisa.teodora16@gmail.com
 */

public class Monomial implements Cloneable {
	private int degree;
	private double coefficient;

	public Monomial() {
		degree = 0;
		coefficient = 0.0;
	}

	public Monomial(int d, double c) {
		this.degree = d;
		this.coefficient = c;
	}

	public int getDegree() {
		return degree;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setDegree(int d) {
		this.degree = d;
	}

	public void setCoefficient(double c) {
		this.coefficient = c;
	}

	public String printMonomial() {
		StringBuffer sBuffer = new StringBuffer();

		if (coefficient < 0.0) {
			sBuffer.append(coefficient);
			if (degree != 0)
				sBuffer.append("x^" + degree);
		} else if (coefficient > 0.0) {
			sBuffer.append("+" + coefficient);
			if (degree != 0)
				sBuffer.append("x^" + degree);
		}
		return sBuffer.toString();
	}

	@Override // the clone method
	public Object clone() throws CloneNotSupportedException {
		Monomial second = new Monomial();
		second.setCoefficient(this.coefficient);
		second.setDegree(this.degree);
		return (Object) second;
	}

}
