package myClasses;
/*
 * @author Narcisa Teodora Sirca 
 * narcisa.teodora16@gmail.com
 */

//relatie de agregare, polinomul ii compus din  monoame

import java.util.ArrayList;

public class Polynomial implements Cloneable {
	// because in this class I override the clone() method
	private ArrayList<Monomial> values; // The list of monomial which form this polynomial
	private int degreeOfPoly;

	public Polynomial() {// empty constructor
		degreeOfPoly = 0;
		values = new ArrayList<Monomial>();
	}

	public Polynomial(ArrayList<Monomial> list) {
		// constructor having as parameter a list of Monomials, which will form its
		// values, after that we set the degree
		this.values = list;
		updateDegree();
	}

	public void updateDegree() {
		// if we had added a monomial with a degree larger that the initial
		// degreeOfPoly, we have to rethink the degree
		for (Monomial it : values) {
			if (it.getDegree() > degreeOfPoly)
				degreeOfPoly = it.getDegree();
		}
	}

	public int getDegree() {
		return degreeOfPoly; // getter for the degree of a polynomial
	}

	public ArrayList<Monomial> getValues() {
		return values; // getter for the monomials of a polynomial
	}

	@Override // the clone method in order to perform some operations without affecting the
				// terms
	public Object clone() throws CloneNotSupportedException {
		Polynomial aux = new Polynomial();
		for (Monomial m : this.values) { // for loop for copying each Monomial
			Monomial clona = (Monomial) m.clone();
			aux.addMonomial(clona);
		}
		aux.updateDegree();
		return aux;
	}

	public void addMonomial(Monomial m) {// adding a monomial, parameter=the monomial I need to add to my Polynomial
		boolean test = false;
		for (Monomial iterator : values) {// loop for checking if a monomial of this degree, case in which I add the
											// coefficients
			if (iterator.getDegree() == m.getDegree()) {
				iterator.setCoefficient(iterator.getCoefficient() + m.getCoefficient());
				test = true;
			}
		}
		if (test == false) {
			values.add(m);// no such Monomial was already in my Polynomial, I add this one
			updateDegree();
		}
	}

	public double valueAtDegree(int degree) {
		/// I search for a particular coefficient of the monomial at power= degree
		// int I have doubles as coefficients,
		for (Monomial m : this.values) {
			if (m.getDegree() == degree)
				return m.getCoefficient();
		}
		return 0.0; // no value found
	}

	// adding polynomials
	public Polynomial addPolynomials(Polynomial p2) throws CloneNotSupportedException {
		Polynomial temporaryPA = (Polynomial) this.clone();
		for (Monomial m2 : p2.values) {
			temporaryPA.addMonomial(m2);
		}
		updateDegree();
		// removeZeroMonomial();
		return temporaryPA;
	}

	// subtracting polynomials
	public Polynomial subtractPolynomials(Polynomial p2) throws CloneNotSupportedException {
		Polynomial temporaryPS = (Polynomial) this.clone();
		for (Monomial m2 : p2.values) {
			m2.setCoefficient((-1) * (m2.getCoefficient()));
			temporaryPS.addMonomial(m2);
		}
		updateDegree();
		// removeZeroMonomial();
		return temporaryPS;
	}

	// Multiply polynomials
	public Polynomial multiplyPolynomials(Polynomial p2) {// parameter=the polynomial I multiply with
		Polynomial temporaryPP = new Polynomial();
		for (Monomial m1 : this.values)
			for (Monomial m2 : p2.values) {
				Monomial product = new Monomial(m1.getDegree() + m2.getDegree(),
						m1.getCoefficient() * m2.getCoefficient());
				temporaryPP.addMonomial(product);
			}
		return temporaryPP;// returns a new polynomial, after it is constructed
	}

	/// multiply polynomial by an actually double ( integer) value
	// I need this method in implementing Division, so even if the algorithm should
	/// use integers, this parameter will be composed
	// with values of coefficients of some Monomials, which I choose to set as
	/// double
	public void multiplyByInteger(double number) {
		for (Monomial m : this.values) {
			m.setCoefficient(m.getCoefficient() * number);
		}
	}

	// shiftRightPoly
	// actually here I increase each degree by the same value, looking as I shift it
	// hole to the right
	public Polynomial shiftRight(int value) throws CloneNotSupportedException {
		Polynomial out = new Polynomial();
		out = (Polynomial) this.clone();
		if (value >= 1) {
			for (Monomial m : out.values) {
				m.setDegree(m.getDegree() + value);
			}
		}
		return out; // returns a new Polynomial;

	}

	// divide polys here
	public ArrayList<Polynomial> dividePolynomials(Polynomial denominator) throws CloneNotSupportedException {
		ArrayList<Polynomial> solution = new ArrayList<Polynomial>();
		Polynomial quotient;
		Polynomial reminder;
		if (this.getDegree() < denominator.getDegree()) {
			quotient = new Polynomial(); // this is the quotient so it's 0
			reminder = (Polynomial) this.clone(); // basically the numerator =reminder
		} else // degree(nominator) >= degree(denominator)
		{
			int dNominator = this.getDegree();
			int dDenominator = denominator.getDegree();
			quotient = new Polynomial(); /// here will only remain the reminder
			reminder = (Polynomial) this.clone(); // here will stay the quotient
			while (dNominator >= dDenominator) {
				Polynomial newDenominator = denominator.shiftRight(dNominator - dDenominator);
				reminder.values.add(new Monomial(dNominator - dDenominator,
						(quotient.valueAtDegree(dNominator)) / (newDenominator.valueAtDegree(dNominator))));
				newDenominator.multiplyByInteger(reminder.valueAtDegree(dNominator - dDenominator));
				quotient.subtractPolynomials(newDenominator);
				quotient.updateDegree();
				dNominator = quotient.getDegree();
			} //it does not have errors, nut somehow it never decrease the reminder
		}
		solution.add(quotient);
		solution.add(reminder);
		return solution;
	}

	// differentiating polynomial
	public void differentiatePolynomial() {
		for (Monomial m : this.values) {
			if (m.getDegree() > 0) {
				m.setCoefficient(m.getDegree() * m.getCoefficient());
				m.setDegree(m.getDegree() - 1);
			} else
				m.setCoefficient(0);
		}

	}

	// integrate Polynomial
	public void integratePolynomial() {
		for (Monomial m : this.values) {
			m.setDegree(m.getDegree() + 1);
			m.setCoefficient(m.getCoefficient() / m.getDegree());
		}
	}
///printing polynomials as: +3.0x^2-4.0x^1+1.0
	public String printPolynomial() {
		StringBuffer sBuffer = new StringBuffer();
		for (Monomial m : this.values) {

			sBuffer.append(m.printMonomial());
		}
		return sBuffer.toString();
	}

}
