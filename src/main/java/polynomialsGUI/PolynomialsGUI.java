package polynomialsGUI;

import myClasses.Polynomial;
import myClasses.Monomial;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//packet separat ar trebui : 1 model 2 view 3 controller, legaturA 1 2...	
/*
 * @author Narcisa Teodora Sirca 
 * narcisa.teodora16@gmail.com
 */

// 6.0x^1-4.0      3.0x^2-6.0x^1+4.0

public class PolynomialsGUI {
	private JFrame ffe;
	private JTextField polynomial1;
	private JTextField polynomial2;
	private JTextField expresie;
	private JTextField rezultat;
	private JLabel poly1 = new JLabel("First Polynomial:"); /// label p1
	private JLabel poly2 = new JLabel("Second Polynomial:"); /// label 2
	private JLabel polyresult = new JLabel("Result of the Operation:");
	private JLabel expression = new JLabel("Expression of the operation that's executed");// pe asta il afisez in casuta
	private JLabel standard = new JLabel("Polynomial must be in this form: cnX^n+cn-1X^n-1+...+c0X^0");
	private JButton deriv1 = new JButton(" P1' "); // buton 1 1
	private JButton integr1 = new JButton(" P1 dx"); // buton 1 2
	private JButton deriv2 = new JButton(" P2' "); // buton 2 1
	private JButton integr2 = new JButton(" P2 dx"); // buton 2 2
	private JButton sum = new JButton("P1+P2"); // sum button
	private JButton diff = new JButton("P1-P2");// subtract button
	private JButton product = new JButton("P1*P2");// multiply button
	private JButton division = new JButton("P1 / P2");// divide button

	/// constructor of this class
	public PolynomialsGUI() throws CloneNotSupportedException {
		initialize();
	}

	private void initialize() throws CloneNotSupportedException {

		ffe = new JFrame("Polynomials operations SNT"); // first frame ever
		ffe.setVisible(true);
		ffe.setSize(800, 600);
		ffe.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		ffe.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		ffe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ffe.getContentPane().setLayout(null);

		/// settings for first lable
		ffe.add(poly1);
		poly1.setBounds(40, 20, 160, 20);
		poly1.setFont(new Font("Serif", Font.PLAIN, 18));
		poly1.setForeground(new Color(0x0033cc));

		// settings for first polynomial's textField
		polynomial1 = new JTextField();
		rezultat = new JTextField();/// text1
		polynomial1.setBounds(40, 47, 300, 50);
		ffe.getContentPane().add(polynomial1);
		polynomial1.setColumns(500);
		polynomial1.setText("  first polynomial here");

		// ActionListener for P1' button
		deriv1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Polynomial first = new Polynomial();
				String s = polynomial1.getText(); // in s am primul rand
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(s);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				first.differentiatePolynomial();// P1'
				expresie.setText("     P1'");
				rezultat.setText(first.printPolynomial());
				System.out.println(first.printPolynomial());
			}
		});
		deriv1.setBounds(360, 47, 80, 50);
		ffe.getContentPane().add(deriv1);

		// ActionListener for P1 dx button
		integr1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Polynomial first = new Polynomial();
				String s = polynomial1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(s);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				first.integratePolynomial();// P1 dx
				expresie.setText("     P1 dx");
				rezultat.setText(first.printPolynomial());
			}
		});
		integr1.setBounds(460, 47, 80, 50);
		ffe.getContentPane().add(integr1);

		// settings for 2nd polynomial label
		ffe.add(poly2);
		poly2.setBounds(40, 100, 160, 20);
		poly2.setFont(new Font("Serif", Font.PLAIN, 18));
		poly2.setForeground(new Color(0x0033cc));

		// 2nd polynomial textField
		polynomial2 = new JTextField(); // text 2
		polynomial2.setBounds(40, 127, 300, 50);
		ffe.getContentPane().add(polynomial2);
		polynomial2.setColumns(100);
		polynomial2.setText("  second polynomial here");

		// button P2'
		deriv2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Polynomial second = new Polynomial();
				String s = polynomial2.getText(); // in s am al 2 lea polinom
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(s);
				while (m.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				second.differentiatePolynomial();// P2'
				expresie.setText("     P2'");
				rezultat.setText(second.printPolynomial());
			}
		});
		deriv2.setBounds(360, 127, 80, 50);
		ffe.getContentPane().add(deriv2);

		// BUTTON FOR P2 dx
		integr2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final Polynomial second = new Polynomial();
				String s = polynomial2.getText();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(s);
				while (m.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				second.integratePolynomial();// P2 dx
				expresie.setText("     P2 dx");
				rezultat.setText(second.printPolynomial());
			}
		});
		integr2.setBounds(460, 127, 80, 50);
		ffe.getContentPane().add(integr2);
		// expresion settings
		ffe.add(expression); // text casuta
		expression.setBounds(40, 180, 400, 20);
		expression.setFont(new Font("Serif", Font.PLAIN, 18));
		expression.setForeground(new Color(0x0033cc));

		expresie = new JTextField();
		expresie.setBounds(130, 205, 100, 50);
		ffe.getContentPane().add(expresie);
		expresie.setColumns(100);
		expresie.setText("  polynomial expr");

		// settings for result label
		ffe.add(polyresult);
		polyresult.setBounds(40, 260, 200, 20);
		polyresult.setFont(new Font("Serif", Font.PLAIN, 18));
		polyresult.setForeground(new Color(0x0033cc));

		// settings for result textFiled
		rezultat.setBounds(40, 285, 300, 50);
		ffe.getContentPane().add(rezultat);
		rezultat.setColumns(100);
		rezultat.setText("  polynomial result");

		// ActionListener for sum button
		sum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// first polynomial here
				String pol1 = polynomial1.getText();
				final Polynomial first = new Polynomial();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(pol1);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				// second polynomial here
				String pol2 = polynomial2.getText();
				final Polynomial second = new Polynomial();
				Matcher m2 = p.matcher(pol2);
				while (m2.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m2.group(2)), Integer.parseInt(m2.group(1))));
				}
				Polynomial sumalor = null;
				try {

					sumalor = first.addPolynomials(second);
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				rezultat.setText(sumalor.printPolynomial());
				expresie.setText("  P1+P2");
			}
		});
		sum.setBounds(40, 360, 100, 50);
		ffe.getContentPane().add(sum);

		// ActionListener for subtract button
		diff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// first polynomial here
				String pol1 = polynomial1.getText();
				final Polynomial first = new Polynomial();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(pol1);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				// second polynomial here
				String pol2 = polynomial2.getText();
				final Polynomial second = new Polynomial();
				Matcher m2 = p.matcher(pol2);
				while (m2.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m2.group(2)), Integer.parseInt(m2.group(1))));
				}
				Polynomial diferenta = null;
				try {

					diferenta = first.subtractPolynomials(second);
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				rezultat.setText(diferenta.printPolynomial());
				expresie.setText("  P1-P2");
			}
		});
		diff.setBounds(40, 430, 100, 50);
		ffe.getContentPane().add(diff);

		// ActionListener for multiply button P1*P2
		product.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// first polynomial here
				String pol1 = polynomial1.getText();
				final Polynomial first = new Polynomial();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(pol1);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				// second polynomial here
				String pol2 = polynomial2.getText();
				final Polynomial second = new Polynomial();
				Matcher m2 = p.matcher(pol2);
				while (m2.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m2.group(2)), Integer.parseInt(m2.group(1))));
				}
				Polynomial produsul = first.multiplyPolynomials(second);
				rezultat.setText(produsul.printPolynomial());
				expresie.setText("    P1 * P2");
			}
		});
		product.setBounds(170, 360, 100, 50);
		ffe.getContentPane().add(product);

		// ActionListener for divide button

		division.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// first polynomial here
				String pol1 = polynomial1.getText();
				final Polynomial first = new Polynomial();
				Pattern p = Pattern.compile("(-?\\b\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(pol1);
				while (m.find()) {
					first.addMonomial(new Monomial(Integer.parseInt(m.group(2)), Integer.parseInt(m.group(1))));
				}
				// second polynomial here
				String pol2 = polynomial2.getText();
				final Polynomial second = new Polynomial();
				Matcher m2 = p.matcher(pol2);
				while (m2.find()) {
					second.addMonomial(new Monomial(Integer.parseInt(m2.group(2)), Integer.parseInt(m2.group(1))));
				}

				ArrayList<Polynomial> impartirea = new ArrayList<Polynomial>();
				try {
					impartirea = first.dividePolynomials(second);
				} catch (CloneNotSupportedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				StringBuffer imparte = new StringBuffer("q: ");
				for (Polynomial iter : impartirea) {
					imparte.append(iter.printPolynomial() + " r: ");
				}
				imparte.delete(imparte.length() - 3, imparte.length()); /// remove the second "r: "
				rezultat.setText(imparte.toString());
				// rezultat.setText("This operation doesn'work. sorry for the inconvinience");
				expresie.setText("    P1 / P2");
			}
		});
		division.setBounds(170, 430, 100, 50);
		ffe.getContentPane().add(division);
		/// setting for standard
		ffe.add(standard);
		standard.setBounds(40, 500, 600, 20);
		standard.setFont(new Font("Serif", Font.PLAIN, 18));
		standard.setForeground(new Color(0xff0066));

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PolynomialsGUI window = new PolynomialsGUI();
					window.ffe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
