package PT2018.demo.Assignment_1;

import myClasses.Polynomial;
import myClasses.Monomial;
import java.util.ArrayList;

import org.junit.Test;

import junit.framework.TestCase;

public class TestPolynomials extends TestCase {
	public void setUp() {
	}

	@Test
	public void testConstructor() {
		ArrayList<Monomial> array = new ArrayList<Monomial>();
		array.add(new Monomial(1, 2.0));
		;// 2.0x^1
		array.add(new Monomial(0, -3.0));// -3.0x^0
		Polynomial pol = new Polynomial(array);
		assertEquals(pol.printPolynomial(), "+2.0x^1-3.0");
		// 2.0x^1-3.0x^0
	}

	@Test
	public void testPrintPolynomial() {
		Polynomial pol = new Polynomial();
		pol.addMonomial(new Monomial(1, 2.0));// 2.0x^1
		pol.addMonomial(new Monomial(0, -3.0));// -3.0x^0
		assertEquals(pol.printPolynomial(), "+2.0x^1-3.0");
		// 2.0x^1-3.0x^0
	}

	@Test
	public void testAddition() throws CloneNotSupportedException {

		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);

		ArrayList<Monomial> array2 = new ArrayList<Monomial>();
		array2.add(new Monomial(1, 2.0));
		;// 2.0x^1
		array2.add(new Monomial(0, -3.0));// -3.0x^0
		Polynomial second = new Polynomial(array2);
		Polynomial sum = first.addPolynomials(second);
		assertEquals(sum.printPolynomial(), "+3.0x^2-2.0x^1-2.0");
		// 3.0x^2-4.0x^1+1.0x^0
		// 2.0x^1-3.0x^0
		// 3.0x^2-2.0x^1-2.0x^0 sum

	}

	@Test
	public void testDifference() throws CloneNotSupportedException {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		ArrayList<Monomial> array2 = new ArrayList<Monomial>();
		array2.add(new Monomial(1, 2.0));
		;// 2.0x^1
		array2.add(new Monomial(0, -3.0));// -3.0x^0
		Polynomial second = new Polynomial(array2);
		// 2.0x^1-3.0x^0
		Polynomial diff = first.subtractPolynomials(second);
		assertEquals(diff.printPolynomial(), "+3.0x^2-6.0x^1+4.0");
		// 3.0x^2-4.0x^1+1.0x^0
		// 2.0x^1-3.0x^0
		// 3.0x^2-6.0x^1+4.0x^0 difference
	}

	@Test
	public void testProduct() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		ArrayList<Monomial> array2 = new ArrayList<Monomial>();
		array2.add(new Monomial(1, 2.0));
		;// 2.0x^1
		array2.add(new Monomial(0, -3.0));// -3.0x^0
		Polynomial second = new Polynomial(array2);
		// 2.0x^1-3.0x^0
		Polynomial produs = first.multiplyPolynomials(second);
		assertEquals(produs.printPolynomial(), "+6.0x^3-17.0x^2+14.0x^1-3.0");
	}

	@Test
	public void testDifferentiating() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		first.differentiatePolynomial();
		assertEquals(first.printPolynomial(), "+6.0x^1-4.0");
	}

	@Test
	public void testIntegrating() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		first.integratePolynomial();
		assertEquals(first.printPolynomial(), "+1.0x^3-2.0x^2+1.0x^1");
	}

	@Test
	public void testCloning() throws CloneNotSupportedException {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		Polynomial clona = (Polynomial) first.clone();
		clona.getValues().remove(new Monomial(1, -4.0));

		assertEquals(first.printPolynomial(), "+3.0x^2-4.0x^1+1.0");
	}

	@Test
	public void testGetValues() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		// 3.0x^2-4.0x^1+1.0x^0
		assertEquals(first.getValues(), array1);
	}

	@Test
	public void testMultiplyByInteger() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		first.multiplyByInteger(2.0);
		// 3.0x^2-4.0x^1+1.0x^0
		assertEquals(first.printPolynomial(), "+6.0x^2-8.0x^1+2.0");
	}

	@Test
	public void testUpdateDegree() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(1, -4.0));// -4.0x^1
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		first.updateDegree();
		// 3.0x^2-4.0x^1+1.0x^0
		assertEquals(first.getDegree(), 2);
	}

	@Test
	public void testValueAtDegree() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		Polynomial first = new Polynomial(array1);
		double val = first.valueAtDegree(2);
		// 3.0x^2-4.0x^1+1.0x^0
		assertEquals(val, 3.0);
	}

	public void testAddMonomial() {
		ArrayList<Monomial> array1 = new ArrayList<Monomial>();
		array1.add(new Monomial(2, 3.0));// 3.0x^2
		array1.add(new Monomial(0, 1.0));// 1.0x^0
		Polynomial first = new Polynomial(array1);
		Monomial m = new Monomial(1, -4.0);
		first.addMonomial(m);
		// 3.0x^2-4.0x^1+1.0x^0
		assertEquals(first.getValues().contains(m), true);
	}

	public void testShiftRightPolynomial() throws CloneNotSupportedException {
		Polynomial first = new Polynomial();
		first.addMonomial(new Monomial(2, 3.0));// 3.0x^2
		first.addMonomial(new Monomial(0, 1.0));// 1.0x^0
		Polynomial second = first.shiftRight(2);
		// + 3.0x^2+1.0x^0
		// +3.0x^4+1.0x^2
		assertEquals(second.printPolynomial(), "+3.0x^4+1.0x^2");
	}

	/*
	 * 
	 * ArrayList<Polynomial> dividePolynomials(Polynomial denominator) throws CloneNotSupportedException
	 * 
	 */

}
